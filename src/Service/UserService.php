<?php

namespace App\Service;

use App\Repository\UserRepository;

class UserService
{
    const ALPHABET = 'abcdefghijklmnopqrstuvwxyz';
    const PEPPER = 'winjoghkjhu';
    const ENCRYPT_METHOD = 'AES-128-CBC';
    const HASH_METOHD = 'sha3-224';

    private $repo;

    public function __construct()
    {
        $this->repo = new UserRepository;
    }


    public function addUser(string $userName, string $password, string $mail)
    {
        if (! $this->isUser($userName)) {
            $salt = str_shuffle(self::ALPHABET);
            $hashedPassword = $this->hashPassword($password, $salt);
            $encryptedMail = $this->addSpices($mail, $salt);
            $this->repo->addUser($userName, $hashedPassword, $salt, $encryptedMail);
            return true;
        }
        return false;
    }

    public function isUser(string $userName)
    {
        $isUser = $this->repo->isUser($userName);
        return $isUser == 1 ? true : false;
    }

    public function checkAuth(string $userName, string $passwordToTest)
    {
        $hashAndSalt = $this->repo->getHashedPasswordAndSalt($userName);
        if (array_key_exists('hash', $hashAndSalt) & array_key_exists('salt', $hashAndSalt)) {
            $hash = $hashAndSalt['hash'];
            $salt = $hashAndSalt['salt'];
            $hashToTest = $this->hashPassword($passwordToTest, $salt);
            if($hash == $hashToTest){
                return $this->decrypt($this->repo->getSpicedMail($userName), $salt);
            }
            return $hash == $hashToTest;
        }
        return false;
    }

    public function hashPassword($password, $salt)
    {
        $spicedPass = $this->addSpices($password, $salt);
        return hash(self::HASH_METOHD, $spicedPass);
    }

    public function addSpices($toSpice,$salt){
        $salted = openssl_encrypt($toSpice, self::ENCRYPT_METHOD ,$salt);
        return openssl_encrypt($salted, self::ENCRYPT_METHOD ,self::PEPPER);
    }
    
    public function decrypt($toDecript,$salt){
        $unPepper = openssl_decrypt($toDecript, self::ENCRYPT_METHOD, self::PEPPER);
        return openssl_decrypt($unPepper, self::ENCRYPT_METHOD,$salt);
    }
}
