<?php

namespace App\Repository;

use PDO;

class UserRepository
{
    private $connexion;

    public function __construct (){
        $this->connexion = new PDO(
            'mysql:dbname=exo_securite;host=127.0.0.1;port=3306',
            'simplon',
            '1234'
        );
    }

    public function addUser(string $userName, string $hashedPassword, string $salt,string $mail){
        $sql =  'INSERT INTO `user` (`user_login`, `user_password`, `user_salt`, `user_mail`) VALUES (:user_login, :user_password, :user_salt, :user_mail)';
        $stmt = $this->connexion->prepare($sql);
        $stmt->bindParam(':user_login', $userName);
        $stmt->bindParam(':user_password', $hashedPassword);
        $stmt->bindParam(':user_salt', $salt);
        $stmt->bindParam(':user_mail', $mail);
        $stmt->execute();
    }
    public function getHashedPasswordAndSalt(string $userName){
        $sql =  'SELECT `user_password` AS hash, `user_salt` AS salt FROM `user` WHERE `user_login` = :user_login';
        $stmt = $this->connexion->prepare($sql);
        $stmt->bindParam(':user_login', $userName);
        $stmt->execute();
        return $stmt->fetch();
    }

    public function getSpicedMail(string $userName){
        $sql =  'SELECT `user_mail` AS mail, `user_salt` AS salt FROM `user` WHERE `user_login` = :user_login';
        $stmt = $this->connexion->prepare($sql);
        $stmt->bindParam(':user_login', $userName);
        $stmt->execute();
        return $stmt->fetch()['mail'];
    }

    public function isUser(string $userName){
        $sql = 'SELECT count(*) AS nb FROM `user` WHERE `user_login` = :user_login';
        $stmt = $this->connexion->prepare($sql);
        $stmt->bindParam(':user_login', $userName);
        $stmt->execute();
        return $stmt->fetch()['nb'];
    }
}
