<?php

use App\Repository\UserRepository;
use App\Service\UserService;

require __DIR__ . '/../vendor/autoload.php';

$repo = new UserRepository;
$service = new UserService;

$message = '';
$isLoged = false;

if (array_key_exists('log_user', $_POST) & array_key_exists('log_pass', $_POST)) {
    $check = $service->checkAuth($_POST['log_user'], $_POST['log_pass']);
    if ($check) {
        $message = "Your mail is : " . $check;
        $isLoged = true;
    }else{
        $message = "Echec d'authentification";
    }
}

if (array_key_exists('new_user', $_POST) & array_key_exists('new_pass', $_POST) & array_key_exists('new_mail', $_POST)) {
    if ($service->addUser($_POST['new_user'], $_POST['new_pass'], $_POST['new_mail'])) {
        $message = "Welcome";
        $isLoged = true;
    }else{
        $message = "Ce nom existe déjà";
    }
}


// affichage
echo '
<h1>Hello</h1>';

if($message)
{
    echo '<h2>' . $message . '</h2>';
}

if(! $isLoged){
    echo '
<h3>connexion</h3>
<form method = "POST">
    <label for = "log_user">nom</label>
    <input type="text" id="log_user" name="log_user" required >
    <label for = "log_pass">Mot de passe</label>
    <input type="password" id="log_pass" name="log_pass" required >
    <input type ="submit">
</form>

<h3>Enregistrement</h3>
<form method = "POST">
    <label for = "new_user">Nom</label>
    <input type="text" id="new_user" name="new_user" required >
    <label for = "new_pass">Mot de passe</label>
    <input type="password" id="new_pass" name="new_pass" required >
    <label for = "new_mail">Email</label>
    <input type="mail" id="new_mail" name="new_mail" required >
    <input type ="submit">
</form>
';
}
