DROP DATABASE  IF EXISTS exo_securite;
CREATE DATABASE exo_securite;
USE exo_securite;

CREATE TABLE user (
    id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    user_login VARCHAR(25) NOT NULL,
    user_password VARCHAR(255) NOT NULL,
    user_salt VARCHAR(255) NOT NULL,
    user_mail VARCHAR(255) NOT NULL,
    UNIQUE KEY(user_login)
    );
